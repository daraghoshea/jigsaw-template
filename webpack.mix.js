const argv = require('yargs').argv
const command = require('node-cmd')
const mix = require('laravel-mix')
const OnBuild = require('on-build-webpack')
const Watch = require('webpack-watch')
const glob = require('glob-all')
const PurgecssPlugin = require('purgecss-webpack-plugin')
const webpack = require('webpack')

const env = argv.e || argv.env || 'local'
const plugins = [
    new OnBuild(() => {
        command.get('./vendor/tightenco/jigsaw/jigsaw build ' + env, (error, stdout, stderr) => {
            if (error) {
                console.log(stderr)
                process.exit(1)
            }
            console.log(stdout)
        })
    }),
    new Watch({
        paths: ['source/**/*.md', 'source/**/*.php', '*.php', '*.js'],
        options: { ignoreInitial: true }
    }),
]

mix.webpackConfig({ plugins })
mix.setPublicPath('source/assets')



class TailwindExtractor {
    static extract(content) {
        return content.match(/[A-z0-9-:\/]+/g) || [];
    }
}

// Add PurgeCss to production build to reduce tailwindcss file size
if( mix.inProduction() ) {
    mix.webpackConfig({
        plugins: [
            new PurgecssPlugin({

                // Specify the locations of any files you want to scan for class names.
                paths: glob.sync([
                    path.join(__dirname, "source/**/*.blade.php"),
                ]),
                extractors: [
                    {
                        extractor: TailwindExtractor,

                        // Specify the file extensions to include when scanning for
                        // class names.
                        extensions: ["html", "js", "php", "vue"]
                    }
                ],
                whitelist: []
            }),
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development')
                }
            })
        ]
    });
}

mix
    .js('source/_assets/js/app.js', 'source/js')
    .sass('source/_assets/sass/main.scss', 'source/css')
    .options({
        processCssUrls: false,
        postCss: [
            require('tailwindcss')('tailwind/tailwind.config.js'),
            require('autoprefixer')
        ]
    })
    .version()

