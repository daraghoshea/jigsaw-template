<?php

return [
    'baseUrl' => '',
    'production' => false,

    /** Meta */
    'title_default' => 'Site Title',
    'title_append' => ', Site Title', // for pages that have a title, but want a default appended to it
    'meta_description_default' => 'Site Description',   // A default one, not necessarily good SEO practice though
    'img_default' => '',    // Relative url, eg. `/img/default.png`. Leave empty for no default,

    /** Fonts */
    'google_fonts' => [
        // 'Open+Sans:400,600,800'
    ],

    /** Contact */
    'contact' => [
        'email' => '',
        'telephone' => '',
        'address' => ''
    ],

    /** Social Media Accounts */
    'social_accounts_urls' => [
        'twitter' => 'https://www.twitter.com/roundrobotco',        // eg. https://www.twitter.com/roundrobotco
        'facebook' => '',
        'instagram' => '',
        'tripadvisor' => '',
        'pinterest' => ''
    ],

    /** Open Graph - set to `false` if not to be included */
    'include_open_graph' => true,
    'og_img_default' => '',
    'og_img_w_default' => '',
    'og_img_h_default' => '',

    /** Twitter Meta */
    'include_twitter_meta' => true,
    'twitter_handle' => '',     // eg. `@roundrobotco`

    /** Sitemap */
    'sitemap' => false,                     // Set to false to prevent running
    'sitemap_ignore' => ['/robots.txt'],    // Any files should be ignored
    'sitemap_ping_google' => false,         // Ping google in production when sitemap generated

    /** helper methods */
    'getTitle' => function($page) {
        $title = $page->title;
        // Cannot do dire
        if( ! empty($title) ) {
            return $title . $page->title_append_default;
        }

        return $page->title_default;
    },
    'getAbsolutePath' => function($page, $path = '') {
        return rtrim($page->baseUrl, '/') . "/" . ltrim( $path ?: $page->getPath(), '/' );
    },
    'host' => function($page, $url) {
      return parse_url($url, PHP_URL_HOST);
    },
    'firstOf' => function($page, ...$keys) {
        $firstKeyWithValue = collect($keys)->first(function($key) use ($page) {
            return ! empty($page->get($key));
        });

        return $firstKeyWithValue ? $page->get($firstKeyWithValue) : null;
    }
];