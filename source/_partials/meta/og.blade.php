@if( $page->include_open_graph )
    <!-- Open Graph Data -->
    <meta property="og:title" content="{{ $page->getTitle() }}">
    <meta property="og:description" content="{{ $page->firstOf('og_description', 'description', 'meta_description_default') }}" />
    <meta property="og:image" content="{{ $page->baseUrl }}{{ $page->firstOf('og_img', 'img', 'img_default') }}">
    <meta property="og:image:width" content="{{ $page->firstOf('og_img_w', 'og_img_w_default') }}">
    <meta property="og:image:height" content="{{ $page->firstOf('og_img_h', 'og_img_h_default') }}">
    <meta property="og:url" content="{{$page->getAbsolutePath()}}">
@endif