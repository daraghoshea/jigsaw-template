<?php

use BladeSvg\Svg;
use BladeSvg\SvgFactory;

if( ! function_exists('svg_factory') ) {
    function svg_factory() : SvgFactory
    {
        static $factory;

        if (!$factory) {
            $factory = new SvgFactory([
                'svg_path' => __DIR__ . '/source/_assets/svg'
            ]);
        }

        return $factory;
    }
}

if( ! function_exists('svg_img') ) {
    function svg_img($icon, $class = '', $attrs = []) : Svg
    {
        return svg_factory()->svg($icon, $class, $attrs);
    }
}

return [
    'svg' => function($expression) {
        return "<?php echo e(svg_img($expression)); ?>";
    }
];