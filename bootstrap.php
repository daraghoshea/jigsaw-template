<?php

use TightenCo\Jigsaw\Jigsaw;

/** @var $container \Illuminate\Container\Container */
/** @var $jigsaw Jigsaw */

/** See also `/blade.php` for some helper functionality and `Blade` directives */


/** Macro to make it easier to filter out empty strings from collection */
\Illuminate\Support\Collection::macro('rejectEmpty', function(){
    return $this->filter(function($item){
        return ! empty($item);
    });
});


$events->beforeBuild(function(Jigsaw $jigsaw){

    /** Don't allow to proceed if not in `local` env and baseUrl not set. Sitemap generator will throw an error after build. */
    if( $jigsaw->getEnvironment() != 'local' && trim($jigsaw->getConfig('baseUrl')) === '' ) {
        throw new \RuntimeException("The baseUrl config is not set for `{$jigsaw->getEnvironment()}`");
    }

});

/** Listener that runs after the build to generate a sitemap */
$events->afterBuild(App\Listeners\GenerateSitemap::class);