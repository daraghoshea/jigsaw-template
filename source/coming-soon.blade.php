---
title: Coming Soon
---

@extends('_layouts.coming-soon')

@section('page')
    @if($page->img_default)
        <div class="">
            <img src="{{$page->img_default}}" class="max-w-full" alt="{{$page->title}}" />
        </div>
    @endif

    <h1 class="text-center">Coming Soon</h1>

    @if( $page->social_accounts_urls->rejectEmpty()->count() )
        <ul class="flex flex-wrap justify-center list-reset mt-12">
        @foreach($page->social_accounts_urls->rejectEmpty() as $type => $url)
            <li class="mx-4">
                <a href="{{$url}}" target="_blank" rel="nofollow" class="text-black hover:text-grey-dark">
                    @svg("simpleicons.{$type}", "fill-current h-6")
                </a>
            </li>
        @endforeach
        </ul>
    @endif
@endsection