<?php namespace App\Listeners;

use TightenCo\Jigsaw\Jigsaw;
use samdark\sitemap\Sitemap;

class GenerateSitemap
{
    /** @var Jigsaw */
    private $jigsaw;

    public function handle(Jigsaw $jigsaw)
    {
        $this->jigsaw = $jigsaw;

        if( $this->jigsaw->getConfig('sitemap') === false ) {
            return;
        }

        $baseUrl = $this->jigsaw->getConfig('baseUrl');

        /** Setting a baseUrl in local env if none exists as generator will throw an error otherwise */
        if( trim($baseUrl) === '' && $jigsaw->getEnvironment() === 'local' ) {
            $baseUrl = 'http://localhost:8000';
        }

        $sitemap = new Sitemap($this->jigsaw->getDestinationPath() . '/sitemap.xml');

        collect($this->jigsaw->getOutputPaths())->each(function ($path) use ($baseUrl, $sitemap) {
            if (! $this->isAsset($path)) {
                $sitemap->addItem($baseUrl . $path, time(), Sitemap::DAILY);
            }
        });

        $sitemap->write();

        // Ping Google with new sitemap
        $this->pingGoogle( trim($baseUrl, '/') . '/sitemap.xml');
    }

    public function isAsset($path)
    {
        $ignore = [
            '/assets',
            '/mix-manifest.json',
            '/manifest.json'
        ];

        return starts_with($path, array_merge(
            $ignore,
            $this->jigsaw->getConfig('sitemap_ignore')->all()
        ));
    }

    private function pingGoogle($sitemapUrl)
    {
        // Do not continue if disabled in configs or not in production
        if( $this->jigsaw->getConfig('sitemap_ping_google') === false || $this->jigsaw->getEnvironment() !== 'production' ) {
            return;
        }

        $urlToPing = "http://google.com/ping?sitemap=" . urlencode($sitemapUrl);

        /** Simple way to perform GET request */
        file_get_contents( $urlToPing );
    }
}