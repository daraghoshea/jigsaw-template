@if( $page->include_open_graph )
    <!-- Twitter meta -->
    <meta name="twitter:title" content="{{ $page->getTitle() }}">
    <meta name="twitter:description" content="{{ $page->firstOf('og_description', 'description', 'meta_description_default') }}" />
    <meta name="twitter:image" content="{{ $page->firstOf( 'twitter_img', 'og_img', 'img', 'img_default') }}">
    <meta name="twitter:url" content="{{$page->getAbsolutePath()}}">
    <meta name="twitter:site" content="{{ "@" . trim($page->twitter_handle, '@') }}">
    <meta name="twitter:card" content="{{$page->twitter_card ?: 'summary'}}">
@endif