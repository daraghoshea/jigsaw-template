<footer class="px-4 py-8 lg:py-12 text-center text-sm mt-auto">
    <p class="opacity-50">
        Made with @svg('feather.heart', "fill-current text-red", ["width"=>"12", "height"=>"12"]) in Dublin by <a href="https://www.roundrobot.co" target="_blank" title="Web Development Services, Dublin" rel="noreferrer,nofollow">Web Design, Dublin</a>.
    </p>
</footer>