<?php

return [
    'baseUrl' => '',
    'production' => true,

    // Sitemap
    'sitemap_ping_google' => true,          // Ping google in production when sitemap generated
];