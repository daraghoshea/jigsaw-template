@if( $page->google_fonts->count() )
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family={{ str_replace(' ','+',$page->google_fonts->implode('|')) }}" rel="stylesheet">
@endif