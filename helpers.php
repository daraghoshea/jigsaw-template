<?php

if( ! function_exists('carbon') )
{
    function carbon($value = null) {
        return $value ? new \Carbon\Carbon($value) : new \Carbon\Carbon;
    }
}

