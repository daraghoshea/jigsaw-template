<header>

    <responsive-menu>

        {{-- slot-scope is required for the ResponsiveMenu component --}}
        <nav slot-scope="{ menuOpen, toggle }" class="flex items-center lg:max-w-xl lg:mx-auto justify-between flex-wrap px-6 {{$class ?? ''}}">

            {{-- Home Link --}}
            <div class="py-4 flex-no-shrink mr-6">
                <a href="/" role="button" aria-label="Home Link" class="font-brand uppercase hover:text-grey-dark">WEBSITE</a>
            </div>

            {{-- Mobile menu open/close buttons --}}
            <div class="block md:hidden">
                <button @click="toggle" class="flex items-center px-3 py-2 border rounded text-grey-dark bg-white border-grey-dark hover:text-grey hover:border-grey">

                {{-- Burger SVG --}}
                <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" v-if="! menuOpen">
                    <title>Menu</title>
                    <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/>
                </svg>

                {{-- X SVG --}}
                <svg class="fill-current h-3 w-3" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg" v-else>
                    <title>Menu Close</title>
                    <path d="M12 0L0 12" transform="translate(1 1)" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M0 0L12 12" transform="translate(1 1)" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>

                </button>
            </div>


            {{-- Non-mobile menu --}}
            <div class="hidden ml-auto md:flex md:items-center">

            </div>


            {{-- Menu to show/hide --}}
            <div class="w-full block flex-grow md:flex md:items-center md:justify-center md:w-auto py-6 border-b border-grey-light v-cloak-hidden" v-show="menuOpen">

                <div class="text-sm md:flex-grow text-center">

                    {{-- Menu Links --}}
                    <a class="flex items-center px-4 py-2 mr-2 mb-2 rounded-full uppercase font-serif text-white font-semibold bg-blue shadow hover:bg-blue-light hover:shadow-md" href="#">
                        Menu Link
                    </a>
                </div>
            </div>

        </nav>

    </responsive-menu>

</header>