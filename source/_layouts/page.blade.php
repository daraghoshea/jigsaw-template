@extends('_layouts.master')

@section('body')

    @include('_partials.header')

    <main role="main" class="block">

        @yield('page')

    </main>

    @include('_partials.footer')

@endsection