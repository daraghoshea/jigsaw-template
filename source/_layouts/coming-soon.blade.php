@extends('_layouts.master')

@section('body')

    <main role="main" class="block min-h-screen flex items-center justify-center">
        <div class="text-center">
            @yield('page')
        </div>
    </main>

@endsection