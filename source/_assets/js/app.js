import Vue from 'vue'

const app = new Vue({
    el: '#app',
    components: {
        'responsive-menu': require('./components/ResponsiveMenu.vue')
    }
})