<!DOCTYPE html>
<html lang="en" class="bg-white antialiased">
<head>

    <title>{{ $page->getTitle() }}</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="{{ $page->firstOf('meta_description', 'description', 'meta_description_default') }}" />
    <meta name="keywords" content="{{ $page->keywords }}" />

    @include('_partials.meta.og')
    @include('_partials.meta.twitter')
    @include('_partials.meta.google-fonts')

    @yield('meta')

    <link rel="stylesheet" href="{{ mix('source/css/main.css') }}">

    @stack('/head')
</head>
<body class="font-source-sans font-normal text-black leading-normal">

<div id="app" class="min-h-screen flex flex-col">

    @yield('body')

</div>

<script src="{{ mix('source/js/app.js') }}"></script>
@stack('/body')

</body>
</html>